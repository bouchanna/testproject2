<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <h2></h2>
    <?php
        if(isset($_POST["username"]) && isset($_POST["password"])){
            $username = $_POST["username"];
            $password = $_POST["password"];
            
            echo '<h2>Your password is:'.$password.'</h2><br>';
            echo '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Title!</strong>' .$username.'
                </div>';                
        }
    ?>
    <div class="container"> 
        <form action="#" method="POST" role="form">
            <legend>Form title</legend>
        
            <div class="form-group">
                <label for="">Username: </label>
                <input type="text" class="form-control"  name="username" placeholder="Enter username">
            </div>
            <div class="form-group">
                <label for="">Password: </label>
                <input type="password" class="form-control" name="password" placeholder="Your password">
            </div>
        
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    
        
        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a>
        <div class="modal fade" id="modal-id">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>
</html>