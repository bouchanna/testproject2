
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>

    

    

    <?php
        $modal =   "<script>
                        $(document).ready(function(){
                            $('#popupModal').modal('show');
                            console.log('hi');
                        });
                    </script>";

        $errors = array();
        $formData = array();
        if(isset($_GET["username"]) && isset($_GET["password"])){
            $u = $_GET["username"];
            $p = $_GET["password"];
            
            if(empty($u)){
                array_push($errors, "Username is required");
            }else{
                $formData["username"] = $u;
            }
            if(empty($p)){
                array_push($errors, "Password is required");
            } else{
                $formData["password"] = $p;
            }

            echo $u." ".$p;
            echo $modal;

        }

        foreach($errors as $err){
            echo '<div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Oop! Error occured!</strong> <br> '.$err.'
                </div>';
        }
    ?>


        
    <a class="btn btn-primary" data-toggle="modal" href='#popupModal'>Trigger modal</a>
    <div class="modal fade" id="popupModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Form data</h4>
                </div>
                <div class="modal-body">
                    <ul>
                        
                    <?php
                        foreach ($formData as $key => $value) {
                           echo "<li> $key : $value </li>";
                        }
                    ?>    
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    
   
   
   
    
    <div class="container">
        <!-- <form action="verify.php" method="GET" role="form"> -->
        <form action="#" method="GET" role="form">
            <legend>Form data</legend>

            <div class="form-group">
                <label for="">Usrename: </label>
                <input type="text" class="form-control" name="username" placeholder="Ente your username" >
            </div>
            <div class="form-group">
                <label for="">Password:</label>
                <input type="password" class="form-control" name="password" placeholder="Your password" >
            </div>

            <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                Submit
            </button>
            
            
            
        </form>
    </div>


    <?php
        $n=4;

        for($k=1; $k<$n; $k++){
            echo "<h1> Hello ".$k." </h1></br>";
        }
    ?>



    


</body>
</html>