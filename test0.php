<?php
    $data = [
        "Toyota" => ["Prius", "CRV", "Highlander", "Camry"], 
        "Lexus" => ["LX 200", "LX GS", "GS 300", "GS 450"],
        "BMW" => ["X7", "XM", "X5", "X4", "iX3"],
        "Hyundai" => ["HB20", "Ioniq 6", "Sonata", "Bayon"]
    ];
    ksort($data);  
    foreach($data as $key => $model){
        echo "<h4> $key </h4>";
        echo "<ul> ";
        foreach($model as $m){
            echo "<li> $m </li>";
        }
        echo "</ul>";
    }
?>


<?php

    $data = [
        "Toyota", "Lexus", "BMW", "Hyundai"
    ];
    sort($data);
    foreach ($data as $item) {
        echo "<ul  style='list-style-type: square;'>
                <li> <h2> $item </h2>  </li>    
            </ul>";
        //echo "<h2> $item </h2>";
    }
?>


<?php
    $books1 = [
        "Basci Web HTML/CSS" => ["author" => "John", "year" => 2022],
        "PHP 101" => ["author" => "Jane", "year" => 2020]
    ];

    foreach($books1 as $key => $value){
        //var_dump($books);
        echo "<h3>". $key. "</h3>";
        foreach($value as $k => $v){
            echo "<p> $k : $v</p>";
        }       
    }  
?>

<?php
    $books1 = [
        ["title" => "Basci Web HTML/CSS", "author" => "John", "year" => 2022],
        ["title" => "PHP 101", "author" => "Jane", "year" => 2020]
    ];

    foreach($books1 as $books){
        //var_dump($books);
        echo "<h1>". $books['title']. "</h2>";
        foreach($books as $book){
            echo "<p> $book<sup>TM </sup> </p>";
        }
    }
?>

<?php
       
    $books2 = array(
        ["title" => "JOhn", "author"=> "John", "year"=> 2022],
        ["title" => "PHP 101", "author" => "Jane", "year" => 2020]
    );

    $books3 = array(
        array("title" => "JOhn", "author"=> "John", "year"=> 2022),
        ["title" => "PHP 101", "author" => "Jane", "year" => 2020]
    );



  

?>
